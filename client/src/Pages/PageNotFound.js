import React from 'react';
import { useLocation } from 'react-router-dom'
function PageNotFound() {
    let location = useLocation()

    return (
        <div className="App">
            sorry about that, the page {location.pathname} doesnt exist !
        </div>
    );
}

export default PageNotFound;
