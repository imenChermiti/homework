import React, { useEffect, useState } from 'react';
import { Form, Button, Alert } from "react-bootstrap";
import "./login.css";
import { useDispatch, useSelector } from 'react-redux';
import { login } from '../../Redux/authSlice';
import { useHistory } from 'react-router';

function Login() {
  const { error, loading, is_auth } = useSelector(state => state.auth)
  let dispatch = useDispatch()
  let history = useHistory()
  useEffect(() => {
    (() => {
      if (is_auth) {

        history.push('/')
      }

    })();
  });
  const [password, setPassword] = useState("")
  const [email, setEmail] = useState("")

  const handleSubmit = () => {
    dispatch(login({ email, password }))
  }


  return (
    <div className="Login">

      <Form >
        {error && <Alert style={{ width: "80%", margin: "10px auto" }} variant={"danger"}>
          {error}
        </Alert>}
        <Form.Group size="lg" controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            autoFocus
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>
        <Form.Group size="lg" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>

        <Button block size="lg" onClick={handleSubmit} className="login_btn" disabled={!(email && password) || loading}>
          Login
        </Button>
      </Form>
    </div>
  );
}

export default Login;