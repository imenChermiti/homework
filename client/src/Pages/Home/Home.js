import React, { useEffect, useState } from 'react';
import { Col, Row, Card, Container, Button } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router';
import data from './data.json'

function Home() {
  const {  user,is_auth } = useSelector(state => state.auth)
  const [photos, setPhotos] = useState([])
  useEffect(() => {
    setPhotos(data)
  }, []);

  return (
    <Container>
      {!is_auth&&<Redirect to='/login'/>}
      < div className="App" >
      <h2>   Hello {user?.username}</h2>
        < div style={{ width: 90 + "vw", margin: "auto" }}>


        </div >
        {photos.map((obj, key) => <Row key={key}>
          <Col md={{ span: 6, offset: 3 }}>


            <Card>

              <Card.Img variant="bottom" src={obj.src} />
              <Card.Body>
                <Card.Text>
                  Some quick example text to build on the card title and make up the bulk
                  of the card's content.
                </Card.Text>
                <Row>
                <Col><Button>Like</Button></Col>
                <Col>
                  <Button>Comment</Button></Col>
                  </Row>
              </Card.Body>
            </Card>
            <br />
          </Col>
        </Row>)}

      </div >

    </Container>
  );
}

export default Home;