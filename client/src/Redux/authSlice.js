import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios'

const initialState = {
    loading: false,
    is_auth: false,
    token:"",
    user: {

    },
    error: ""

}
export const login = createAsyncThunk(
    'auth/login',
    async (user) => {
        try {
            const response = await axios.post("http://localhost:3001/api/login", user)
            return response.data
        }
        catch (error) {
            return error
        }
    }
);
export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        logout: (state, action) => {
            state.user = {}
            state.loading = false
            state.is_auth = false
            state.error = ""

        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(login.pending, (state) => {
                state.error = ''
                state.loading = true
            })
            .addCase(login.fulfilled, (state, action) => {
                state.token = action?.payload?.token;
                state.user = action?.payload?.result;
                state.error = action?.payload?.auth ? " " : " Invalid email or password !"
                state.loading = false
                state.is_auth = action?.payload?.auth ? true : false
            })
            .addCase(login.rejected, (state, action) => {
                state.error = action.error;
                state.loading = false

            });
    },


});
export const { logout } = authSlice.actions;

export default authSlice.reducer;