import React from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import './App.css';
import { Login, Home, PageNotFound } from "./Pages/index";
import Navigation from "./Navigation/Navgation";

function App() {

  return (
    <div className="App">
      <BrowserRouter>
        <Navigation />
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="/" component={Home} />
          <Route path="*" component={PageNotFound} />


        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
