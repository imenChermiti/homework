import React from "react";
import './Navigation.css';
import { Link } from "react-router-dom";
import { Nav, Navbar } from 'react-bootstrap'
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../Redux/authSlice";

function Navigation() {
    let is_auth = useSelector(state => state.auth.is_auth)
    let dispatch = useDispatch()

    return (
        <div >
            <Navbar collapseOnSelect bg="light" expand="md" className="mb-3">
                <Nav.Link to="/">
                    <Navbar.Brand className="font-weight-bold text-muted">
                        MyApp
                    </Navbar.Brand>
                </Nav.Link>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                    <Nav >
                        {is_auth ?
                            <>
                                <Nav.Link as={Link} to="/">Home</Nav.Link>
                                <Nav.Link as={Link} onClick={()=>dispatch(logout())} to="/login">Logout</Nav.Link>
                            </>
                            :
                            <>
                                <Nav.Link as={Link} to="/login">Login</Nav.Link>
                            </>}
                    </Nav>
                </Navbar.Collapse>
            </Navbar>

        </div>
    );
}

export default Navigation;