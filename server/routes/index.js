var express = require('express');
var jwt = require('jsonwebtoken');
var router = express.Router();
var data = require('./users.json');

router.post('/api/login', function (req, res, next) {
  console.log(req.body)

  const email = req.body.email
  const user = data.filter(obj => obj.email === email)

  if (user.length) {
    const id = user[0].id

    const token = jwt.sign({ id }, "myJwtSecret", {
      expiresIn: 300,
    })
    console.log(token);
    res.send({ auth: true, token: token, result: user[0] });

  }
  else res.sendStatus(404);

});

module.exports = router;
